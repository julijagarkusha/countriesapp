import React from "react";
import { Outlet } from "react-router-dom";
import NavLink from "../components/ui/NavLink";
import "./Layout.scss";

const Layout = () => {
  return (
    <div className="Layout">
      <header>
        <NavLink path="/" text="Home" />
        <NavLink path="countries" text="Countries" />
      </header>
      <main>
        <Outlet />
      </main>
    </div>
  );
}

export default Layout;
