import React from "react";
import HomeInfo from "../../../components/common/HomeInfo";
import CountryForm from "../../../components/common/CountryForm";
import Container from "../../../components/ui/Container";
import {useSelector} from "react-redux";
import {selectAllCountriesLoading} from "../../../store/countries/countries.selector";
import Loader from "../../../components/ui/Loader";
import "./Home.scss";

const Home = () => {
  const isCountriesLoading = useSelector(selectAllCountriesLoading);

  return (
    <div className="Home">
      <HomeInfo />
      <Container className="Home-container">
        {isCountriesLoading
          ? <Loader/>
          : <CountryForm />
        }
      </Container>
    </div>
  )
}

export default Home;
