import React from "react";
import CountriesList from "../../../components/common/CountriesList";
import {Typography} from "@mui/material";
import Container from "../../../components/ui/Container";

const Countries = () => {
  return (
    <Container>
      <Typography
        variant="h5"
        component="h2"
        sx={{ pb: 1, color: '#109CF1' }}
      >
        Countries List
      </Typography>
      <CountriesList />
    </Container>
  )
}

export default Countries;
