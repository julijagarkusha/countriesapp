import React, { useState } from "react";
import { useLocation, useParams, Link } from "react-router-dom";
import { Typography } from "@mui/material";
import { useSelector } from "react-redux";
import Container from "../../../components/ui/Container";
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import ControlPointIcon from '@mui/icons-material/ControlPoint';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import ListItemTitle from "../../../components/ui/List/ListItemTitle";
import ListItemSubtitle from "../../../components/ui/List/ListItemSubtitle";
import "./Country.scss";
import {selectAllCountries, selectAllCountriesLoading} from "../../../store/countries/countries.selector";

const Country = () => {
  const { country } = useParams();
  const location = useLocation();
  const params = new URLSearchParams(location.search);
  const isCountriesLoading = useSelector(selectAllCountriesLoading);
  const countries = useSelector(selectAllCountries);
  const [ openCountryName, setOpenCountryName ] = useState(true);
  const [ openCountryLanguages, setOpenCountryLanguages ] = useState(true);
  const [ openCountryTranslations, setOpenCountryTranslations ] = useState(false);

  const getCountryByOfficialName = () => countries.find((countryItem) => countryItem.name.official === country);

  const currentCountry = getCountryByOfficialName();
  const currencies = currentCountry?.currencies;
  const currencyCodes = currencies ? Object.keys(currencies) : [];

  const currencyInfo = currencyCodes.map((currencyCode) => {
    const currency = currencies[currencyCode];
    return {
      code: currencyCode,
      name: currency.name,
      symbol: currency.symbol,
    };
  });

  const onToggleCountryName = () => {
    setOpenCountryName(!openCountryName);
  };

  const onToggleCountryLanguages = () => {
    setOpenCountryLanguages(!openCountryLanguages);
  };

  const onCountryTranslations = () => {
    setOpenCountryTranslations(!openCountryTranslations);
  };

  if (isCountriesLoading || !countries.length) {
    return (<div>LOADING</div>)
  }

  return (
    <Container>
      <Typography
        variant="h5"
        component="h2"
        sx={{ pb: 1, color: '#109CF1', fontWeight: 700 }}
      >
        {currentCountry.translations[params.get('translation')].official}
      </Typography>
      <List
        sx={{ width: '100%', bgcolor: 'background.paper' }}
        component="nav"
        aria-labelledby="nested-list-subheader"
      >
        <ListItemButton onClick={onToggleCountryName} sx={{ py: 0, width: '100%' }} classes={{ root: 'Country-button' }}>
          <ListItemIcon>
            <AddIcon />
          </ListItemIcon>
          <ListItemText primary="Name: " classes={{ root: 'Country-title' }} />
          {openCountryName ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openCountryName} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemSubtitle text="Official: " value={currentCountry.name.official} className='Country-subTitle-warningCustom' />
            <ListItemSubtitle text="Common: " value={currentCountry.name.common} className='Country-subTitle-warningCustom' />
          </List>
        </Collapse>
      </List>
      <ListItemButton sx={{ py: 0 }} classes={{ root: 'Country-button' }}>
        <ListItemIcon>
          <AddIcon />
        </ListItemIcon>
        <ListItemText primary="Currencies: " classes={{ root: 'Country-title' }} />
      </ListItemButton>
      <ListItemButton sx={{ pl: 4, py: 0 }} classes={{ root: 'Country-subTitle-warningCustom' }}>
        <ListItemIcon><AddIcon /></ListItemIcon>
        <ListItemText primary="Nok: " classes={{ root: 'Country-title Country-subTitle' }} />
      </ListItemButton>
      {currencyInfo.map((currency) => (
        <List component="div" sx={{ mb: 1, pb: 0, pt: 0 }} classes={{ root: 'Country-subList Country-subTitle-warning' }} key={currency.name}>
          <ListItemSubtitle text="Name: " value={currency.name} className='Country-subTitle-warning' />
          <ListItemSubtitle text="Symbol: " value={currency.symbol} className='Country-subTitle-warning' />
        </List>
      ))}
      <ListItemTitle text='Capital' className='Country-button' />
      <List component="div" sx={{ pb: 1, pt: 0 }}>
        <ListItemButton sx={{ pl: 4, py: 0 }} classes={{ root: 'Country-subTitle-warningCustom' }}>
          <ListItemIcon><ControlPointIcon /></ListItemIcon>
          <ListItemText primary={currentCountry.capital[0]} classes={{ root: 'Country-text' }} />
        </ListItemButton>
      </List>
      <ListItemButton onClick={onToggleCountryName} sx={{ py: 0, mb: 1 }} classes={{ root: 'Country-button' }}>
        <ListItemIcon><AddIcon /></ListItemIcon>
        <ListItemText primary="Region: " classes={{ root: 'Country-title Country-subTitle' }} />
        <ListItemText primary={currentCountry.region} classes={{ root: 'Country-text' }} />
      </ListItemButton>
      <ListItemButton onClick={onToggleCountryName} sx={{ py: 0, mb: 1 }} classes={{ root: 'Country-button' }}>
        <ListItemIcon><AddIcon /></ListItemIcon>
        <ListItemText primary="Subregion: " classes={{ root: 'Country-title Country-subTitle' }} />
        <ListItemText primary={currentCountry.subregion} classes={{ root: 'Country-text' }} />
      </ListItemButton>
      <ListItemButton onClick={onToggleCountryLanguages} sx={{ py: 0 }} classes={{ root: 'Country-button' }}>
        <ListItemIcon><AddIcon /></ListItemIcon>
        <ListItemText primary="Languages: " classes={{ root: 'Country-title' }} />
        {openCountryLanguages ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openCountryLanguages} timeout="auto" unmountOnExit>
        <List component="div"  sx={{ pb: 1, pt: 0 }} >
          {Object.keys(currentCountry.languages).map((languageCode) => (
            <ListItemSubtitle text={languageCode} value={currentCountry.languages[languageCode]} key={languageCode} className='Country-subTitle-warningCustom' />
          ))}
        </List>
      </Collapse>
      <ListItemTitle text={`Flag: ${currentCountry.flag}`} className='Country-button' />
      <ListItemButton onClick={onCountryTranslations} sx={{ py: 0, mt: 1 }} classes={{ root: 'Country-button' }}>
        <ListItemIcon>
          <AddIcon />
        </ListItemIcon>
        <ListItemText primary="Translations: " classes={{ root: 'Country-title' }} />
        {openCountryTranslations ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openCountryTranslations} timeout="auto" unmountOnExit>
        {Object.entries(currentCountry.translations).map(([languageCode, translation]) => (
          <List key={languageCode} component="div" disablePadding>
            <ListItemButton sx={{ pl: 4, py: 0 }} classes={{ root: 'Country-subTitle-warningCustom' }}>
              <ListItemIcon><AddIcon /></ListItemIcon>
              <ListItemText primary={languageCode} classes={{ root: 'Country-title Country-subTitle' }} />
            </ListItemButton>
            <List component="div" disablePadding classes={{ root: 'Country-subList Country-subTitle-warning' }}>
              <ListItemSubtitle text="Official: " value={translation.official} className='Country-subTitle-warning' />
              <ListItemSubtitle text="Common: " value={translation.common} className='Country-subTitle-warning' />
            </List>
          </List>
        ))}
      </Collapse>
      <Link to="/countries" className="Country-ButtonLink">
        Back to countries
      </Link>
    </Container>
  )
}

export default Country;
