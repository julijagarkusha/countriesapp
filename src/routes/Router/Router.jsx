import { Routes, Route } from "react-router-dom";
import {useDispatch} from "react-redux";
import Main from "../../layouts";
import Home from "../pages/Home";
import Countries from "../pages/Countries";
import Country from "../pages/Country";
import {useEffect} from "react";
import {getCountriesList} from "../../store/countries/countries.requests";

const Router = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCountriesList());
  }, []);

  return (
    <Routes>
      <Route path="/" element={<Main />}>
        <Route index element={<Home />} />
        <Route path="/countries" element={<Countries />} />
        <Route path="countries/:country" element={<Country />} />
        <Route path="*" element={<div>404</div>} />
      </Route>
    </Routes>
  )
}

export default Router;
