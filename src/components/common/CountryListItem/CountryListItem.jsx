import React from "react";
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import ListItemText from '@mui/material/ListItemText';
import {useDispatch} from "react-redux";
import "./CountryListItem.scss";
import {removeCountry} from "../../../store/countries/countries.reducer";

const CountryListItem = (props) => {
  const {
    id,
    name,
    flag
  } = props;
  const dispatch = useDispatch();

  const onDeleteCountry = () => {
    dispatch(removeCountry(id));
  }
  return (
    <ListItem
      secondaryAction={
        <IconButton edge="end" aria-label="delete" onClick={onDeleteCountry}>
          <DeleteIcon color={"error"} sx={{ opacity: .75 }}/>
        </IconButton>
      }
      classes={{ root: 'CountryListItem' }}
    >
      <ListItemAvatar>
        <Avatar classes={{ root: 'CountryListItem-avatar' }}>
          {flag}
        </Avatar>
      </ListItemAvatar>
      <ListItemText primary={name} />
    </ListItem>
  )
}

export default CountryListItem;
