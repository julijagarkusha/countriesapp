import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import { Typography } from "@mui/material";
import { useSelector } from "react-redux";
import Select from "../../ui/Select";
import {selectAllCountries, selectAllCountriesLoading} from "../../../store/countries/countries.selector";

const CountryForm = () => {
  const navigate = useNavigate();
  const isCountriesLoading = useSelector(selectAllCountriesLoading);
  const countries = useSelector(selectAllCountries);

  const [ selectedCountry, setSelectedCountry ] = useState();
  const [ selectedTranslationKey, setSelectedTranslationKey ] = useState(null);

  const onChangeCountryHandler = (event) => {
    const selectedCapital = event.target.value;
    const selectedCountry = countries.find((country) => country.capital[0] === selectedCapital);
    console.log('debug selectedCountry: ', selectedCountry);
    setSelectedCountry(selectedCountry);
    setSelectedTranslationKey(null);
  };

  const onChangeTranslationHandler = (event) => {
    const selectedTranslationKey = event.target.value;

    setSelectedTranslationKey(selectedTranslationKey);
  };

  const onFormSubmitHandler = (event) => {
    event.preventDefault();
    navigate(`/countries/${selectedCountry.name.official}?translation=${selectedTranslationKey}`);
  }

  useEffect(() => {
   const Ukraine = countries.find(country => country.name.official === 'Ukraine');

    setSelectedCountry(Ukraine);
  }, [isCountriesLoading])

  useEffect(() => {
    if (selectedCountry && selectedCountry.translations) {
      const translationKeys = Object.keys(selectedCountry.translations);
      if (translationKeys.length > 0) {
        const defaultTranslationKey = translationKeys[0];
        setSelectedTranslationKey(defaultTranslationKey);
      }
    }
  }, [selectedCountry]);

  if(isCountriesLoading || !selectedCountry) {
    return null;
  }

  return (
    <form className='CountryForm' onSubmit={onFormSubmitHandler}>
      <Typography
        variant="h5"
        component="h2"
        sx={{ pb: 1, color: '#109CF1' }}
      >
        Capital form
      </Typography>
      <Select
        id="selectCapital"
        value={`${selectedCountry.flag} ${selectedCountry.capital[0]}`}
        onChange={onChangeCountryHandler}
        placeholder="Select Capital"
        children={countries.filter((country) => !!country.capital).map((country) => {
          return (
            <MenuItem key={`${country.name.common}_${country?.capital[0]}`} value={country?.capital[0]}>
              {country.flag} {country?.capital[0]}
            </MenuItem>
          )
        })}
      />
      <Select
        id="selectTranslation"
        value={selectedTranslationKey || ''}
        onChange={onChangeTranslationHandler}
        placeholder="Select translation"
        children={selectedCountry ? (
          Object.keys(selectedCountry.translations).map((key) => (
            <MenuItem key={key} value={key}>
              {key}
            </MenuItem>
          ))
        ) : (
          <MenuItem disabled value="">No country selected</MenuItem>
        )}
      />
      <Button
        type="submit"
        variant="contained"
        sx={{ px: 3, py: 1, my: 1, width: '100%', fontWeight: 700 }}
      >
        Read more about {selectedCountry ? selectedCountry.name.official : 'the selected country'}
      </Button>
    </form>
  );
}

export default CountryForm;
