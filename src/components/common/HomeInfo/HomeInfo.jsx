import React from "react";
import { Typography } from "@mui/material";
import PublicIcon from "@mui/icons-material/Public";
import Container from "../../ui/Container";
import "./HomeInfo.scss";

const HomeInfo = () => {
  return (
    <Container className="HomeInfo">
      <Typography
        variant="h5"
        component="h2"
        sx={{ px: 1 }}
      >
        Countries
      </Typography>
      <PublicIcon />
    </Container>
  )
}

export default HomeInfo;
