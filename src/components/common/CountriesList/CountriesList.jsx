import React from "react";
import "./CountriesList.scss";
import {useSelector} from "react-redux";
import CountryListItem from "../CountryListItem";
import {selectAllCountries, selectAllCountriesLoading} from "../../../store/countries/countries.selector";

const CountriesList = () => {
  const isCountriesLoading = useSelector(selectAllCountriesLoading);
  const countries = useSelector(selectAllCountries);

  if (isCountriesLoading) {
    return (<div>LOADING</div>)
  }

  return (
    <div className="CountriesList">
      {countries.map((country) => {
        return (
          <CountryListItem
            key={country.name.official}
            id={country.name.official}
            name={country.name.official}
            flag={country.flag}
          />
        )
      })}
    </div>
  )
}

export default CountriesList;
