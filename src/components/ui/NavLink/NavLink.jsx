import React from "react";
import { NavLink as ReactNavLink } from "react-router-dom";
import Button from "@mui/material/Button";
import "./NavLink.scss";

const  NavLink = (props) => {
  const {
    path,
    text,
  } = props;

  return (
    <Button sx={{ p: 0 }}>
      <ReactNavLink
        to={path}
        className={({ isActive}) => isActive ? "NavLink NavLink-active" : "NavLink"}
      >
        {text}
      </ReactNavLink>
    </Button>
  )
}

export default NavLink;
