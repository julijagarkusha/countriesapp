import React from "react";
import "./Container.scss";

const Container = (props) => {
  const {
    className,
    children
  } = props;

  return (
    <article className={`Container ${className}`}>{children}</article>
  )
}

export default Container;
