import React from "react";
import { Select as MUISelect } from '@mui/material';
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import "./Select.scss";

const Select = (props) => {
  const {
    id,
    value,
    onChange,
    children,
    placeholder
  } = props;

  return (
    <div className='Select'>
      <InputLabel id={id} classes={{ root: 'Select-label' }}>{placeholder}</InputLabel>
      <FormControl>
        <MUISelect
          labelId={id}
          displayEmpty
          value={value}
          onChange={onChange}
          input={<OutlinedInput />}
          renderValue={(selected) => {
            if (Array.isArray(selected)) {
              return selected.join(', ');
            } else if (selected === null || selected === undefined) {
              return '';
            } else {
              return selected;
            }
          }}
          inputProps={{ 'aria-label': 'Without label' }}
          classes={{ root: 'Select-field' }}
        >
          {children}
        </MUISelect>
      </FormControl>
    </div>
  )
}

export default Select;
