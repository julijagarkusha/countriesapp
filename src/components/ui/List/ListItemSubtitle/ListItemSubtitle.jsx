import React from "react";
import ListItemIcon from "@mui/material/ListItemIcon";
import CheckIcon from "@mui/icons-material/Check";
import ListItemText from "@mui/material/ListItemText";
import ListItemButton from "@mui/material/ListItemButton";
import "./ListItemSubtitle.scss";

const ListItemSubtitle = (props) => {
  const {
    text,
    value,
    className,
  } = props;

  return (
    <ListItemButton sx={{ pl: 4, py: 0 }} classes={{ root: className }}>
      <ListItemIcon>
        <CheckIcon />
      </ListItemIcon>
      <ListItemText primary={text} classes={{ root: 'ListItemSubtitle-text ListItemSubtitle' }} />
      <ListItemText primary={value} classes={{ root: 'ListItemSubtitle-value' }} />
    </ListItemButton>
  )
}

export default ListItemSubtitle;
