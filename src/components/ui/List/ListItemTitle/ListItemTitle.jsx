import React from "react";
import ListItemIcon from "@mui/material/ListItemIcon";
import AddIcon from "@mui/icons-material/Add";
import ListItemText from "@mui/material/ListItemText";
import ListItemButton from "@mui/material/ListItemButton";
import "./ListItemTitle.scss";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";

const ListItemTitle = (props) => {
  const {
    text,
    onClickHandler,
    isShowItems,
    className,
  } = props;

  const getCollapseIcon = () => isShowItems ? <ExpandLess /> : <ExpandMore />;

  return (
    <ListItemButton sx={{ py: 0, bgcolor: '#DDEFFF'}} onClick={onClickHandler} classes={{ root: className }}>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary={text} classes={{ root: 'ListItemTitle' }} />
      {isShowItems ? getCollapseIcon() : null}
    </ListItemButton>
  )
}

export default ListItemTitle;
