import { createAsyncThunk } from "@reduxjs/toolkit";
import CountriesApi from "../../api/CountriesApi";

export const getCountriesList = createAsyncThunk(
  "countries/getCountriesList",
  async () => {
    try {
      const response = await CountriesApi.getList();
      return response;
    } catch (exception) {
      return exception;
    }
  }
);
