export const selectAllCountries = (state) => state.countries.list;
export const selectAllCountriesLoading = (state) => state.countries.fetchLoading;
