import { createSlice } from "@reduxjs/toolkit";
import { initialState } from "./countries.state.js";
import { getCountriesList } from "./countries.requests";

export const countriesSlice = createSlice({
  name: 'countries',
  initialState,
  reducers: {
    removeCountry: (state, action) => {
      state.list = state.list.filter((country) => country.name.official !== action.payload);
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getCountriesList.fulfilled, (state, action) => {
      state.fetchLoading = false;
      state.list = action.payload.filter(item => item.capital !== undefined);
    });
    builder.addCase(getCountriesList.pending, (state) => {
      state.fetchLoading = true;
    });
  }
})

export const { removeCountry } = countriesSlice.actions

export default countriesSlice.reducer;
