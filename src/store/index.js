import { createLogger } from "redux-logger";
import { configureStore } from "@reduxjs/toolkit";
import countries from "./countries/countries.reducer";

const logger = createLogger({
  collapsed: true
});

const store = configureStore({
  reducer: {
    countries
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(logger),
});

export default store;
