class CountriesApi {
  static URL = "https://restcountries.com/v3.1";

  static getList () {
    return fetch(`${CountriesApi.URL}/all`)
      .then(response => {
        if(response.ok) {
          return response.json();
        }

        throw new Error('Can not fetch post list from server');
      })
  }

  static getPostById (id) {
    return fetch(`${CountriesApi.URL}/${id}`)
      .then(response => {
        if(response.ok) {
          return response.json();
        }

        throw new Error('Can not fetch post list from server');
      })
  }

  static delete(id) {
    return fetch(CountriesApi.URL + id, {
      method: 'DELETE',
    }).then(response => {
      if(response.ok) {
        return response.json();
      }

      throw new Error('Can not delete post on server')
    })
  }
}

export default CountriesApi;
